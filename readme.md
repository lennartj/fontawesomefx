Kudos to Dave Gandy who has created an incredible cool icon font called [Font Awesome][1].

This font fits perfectly into JavaFX as

* all these beautiful icons are scalable vector graphics
* each icon (unicode character) can be styled with css
* incredible lightweight (one font 519 icons)

#Currently FontAwesome 4.3.0 and WeatherIcons 1.3 are build-in.

#Usage
* [8.3 announcement][10]
* [8.2 announcement][9]
* [8.1 announcement][8]
* [8.0.10 announcement][5]
* [8.0.9 announcement][6]
* [8.0.7/8.0.8 announcement][7]
* [blog post2][3]
* [blog post1][2]

#Maven
    <dependency>
        <groupId>de.jensd</groupId>
        <artifactId>fontawesomefx</artifactId>
        <version>8.3</version>
    </dependency>

#License
FontAwesomeFX is licensed under the [Apache 2.0 license][4].
If this license is not suitable, please contact me to discuss an alternative license.

[1]: http://fortawesome.github.com/Font-Awesome/
[2]: http://www.jensd.de/wordpress/?p=692
[3]: http://www.jensd.de/wordpress/?p=733
[4]: http://www.apache.org/licenses/LICENSE-2.0.html
[5]: http://www.jensd.de/wordpress/?p=1556
[6]: http://www.jensd.de/wordpress/?p=1457
[7]: http://www.jensd.de/wordpress/?p=1182
[8]: http://www.jensd.de/wordpress/?p=1182
[9]: http://www.jensd.de/wordpress/?p=1971
[10]: http://www.jensd.de/wordpress/?p=2002